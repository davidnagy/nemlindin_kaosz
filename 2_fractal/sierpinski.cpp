#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <stdio.h> 
#include <armadillo>
#include <vector>
#include <math.h>

using namespace std;
using namespace arma;

vec direction()
{
	double randn = randu();
	vec d=zeros<vec>(2);
	if (0<=randn && randn<1.0/3.0)
	{
		d[0]=0;
		d[1]=0;
		return d;
	}
	else if(1.0/3.0<=randn && randn<2.0/3.0)
	{
		d[0]=1;
		d[1]=0;
		return d;
	}
		else if(2.0/3.0<=randn && randn<=1)
	{
		d[0]=0.5;
		d[1]=sqrt(3.0)/2.0;
		return d;
	}
	cout <<randn<< endl;
}

int main()
{
	srand (time(NULL));
	int steps=100000;
	mat	state=zeros<mat>(steps,2);

	for (int i=0; i<steps-1; i++)
	{
		state.row(i+1)=(state.row(i)+direction().t())/2;
	}
	//cout << state<< endl;
	state.save("sierpinski.dat", raw_ascii);

	//gnuplot plot
	FILE* gnuplotPipe;
	gnuplotPipe = popen("gnuplot","w");
	fprintf(gnuplotPipe,"plot \"sierpinski.dat\" with dots\n");
    fflush(gnuplotPipe);
    printf("press enter to continue...");        
    getchar();        
    fprintf(gnuplotPipe,"exit \n");  
	return 0;
}