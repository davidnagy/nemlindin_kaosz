#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <stdio.h> 
#include <armadillo>
#include <vector>
#include <math.h>

using namespace std;
using namespace arma;

mat develop_state(mat state)
{
	double randn = randu();

	mat m,v,m1,m2,m3,m4,v1,v2,v3,v4;
	m1 <<0<<0<<endr<<0<<0.16<<endr;
	m2 <<0.85<<0.04<<endr<<-0.04<<0.85<<endr;
	m3 <<0.2<<-0.26<<endr<<0.23<<0.22<<endr;
	m4 <<-0.15<<0.28<<endr<<0.26<<0.24<<endr;
	v1 <<0<<endr<<0<<endr;
	v2 <<0<<endr<<1.6<<endr;
	v3 <<0<<endr<<1.6<<endr;
	v4 <<0<<endr<<0.44<<endr;

	if (randn<=0.01)
	{
		state=m1*state.t()+v1;
		return state.t();
	}
	else if(randn<=0.85+0.01)
	{
		state=m2*state.t()+v2;
		return state.t();
	}
	else if(randn<=0.85+0.01+0.07)
	{
		state=m3*state.t()+v3;
		return state.t();
	}
	else if(randn<=1)
	{
		state=m4*state.t()+v4;
		return state.t();
	}
	cout <<randn<< endl;
}

int main()
{
	srand (time(NULL));
	int steps=100000;

	mat	state=zeros<mat>(steps,2);
	for (int i=0; i<steps-1; i++)
	{
		state.row(i+1)=develop_state(state.row(i));
	}
	//cout << state;
	state.save("fern.dat", raw_ascii);
	//gnuplot plot
	FILE* gnuplotPipe;
	gnuplotPipe = popen("gnuplot","w");
	fprintf(gnuplotPipe,"plot \"fern.dat\" with dots\n");
    fflush(gnuplotPipe);
    printf("press enter to continue...");        
    getchar();        
    fprintf(gnuplotPipe,"exit \n");  
	return 0;
}