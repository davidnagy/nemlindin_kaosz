//
//  main.cpp
//  Logistic map
//
//  compile with
//  g++ -larmadillo -o run -O1 logistic_map.cpp
//  ./run
//
//  Created by David Nagy on 3/16/13.
//  Copyright (c) 2013 David Nagy. All rights reserved.
//

#include <iostream>
#include <armadillo>

using namespace std;
using namespace arma;

    double r=4;
    int steps=1000000;

double develop_state(double state)
{
    double newstate;
    newstate=r*state*(1-state);
    return newstate;
}

int main()
{
    mat state=zeros<mat>(steps);
    state(0)=0.126545124851;


    for (int i=0; i<steps-1; i++)
    {
        state(i+1)=develop_state(state(i));
    }
    
//1. feladat
    double sum;
    sum=0;
    for (int i=0; i<steps-1; i++)
    {
        sum+=log(abs(4-(8*state(i))));
    }
    sum=sum/steps;
    cout << "1. feladat:" << endl << sum << endl;

//2. feladat
    state.save("logr4.dat", raw_ascii);
    FILE* gnuplotPipe;
    gnuplotPipe = popen("gnuplot","w");
    fprintf(gnuplotPipe,"f(x)=1/(pi*sqrt(x*(1-x)))*10000\n");
    fprintf(gnuplotPipe,"binwidth=0.01 \n");
    fprintf(gnuplotPipe,"bin(x,width)=width*floor(x/width) \n");
    fprintf(gnuplotPipe,"set style fill solid 0.5 \n");
    fprintf(gnuplotPipe,"plot \"logr4.dat\" using (bin($1,binwidth)):(1.0) smooth freq with boxes, f(x) lw 3\n");
    fflush(gnuplotPipe);
    printf("press enter to close...");        
    getchar();        
    fprintf(gnuplotPipe,"exit \n");
    return 0;
}
