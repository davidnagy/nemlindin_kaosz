// kaosz es nem lin.din. 5. feladat - Rudd Balazs
// logisztikus lekepezes - logistic.cpp 
/*
 * forditas: g++ -o fajlnev logistic.cpp -lgsl -lgslcblas -lm
 * abrazolas: gnuplot -persist plot_hist.gp
 * N az iteraciok szama, BINS pedig a binek szama a hisztogramban
 */
#include <iostream>
#include <cmath>
#include <fstream>
#include <sstream>
#include <ctime>
#include <cstdlib>
#include <gsl/gsl_histogram.h>

#define N 1000000
#define BINS 100
using namespace std;

int i;
double x[N];
double log_sum=0;
double avg,rnd;



int main()
{
//     logisztikus lekepezes N iteracioval, veletlen  kezdopontbol------------
    srand((unsigned)time(NULL));
    rnd = (float) rand()/RAND_MAX;
    x[0] = rnd;
    
    for(i=0; i<N-1; i++)
    {
        x[i + 1] = 4*x[i]*(1 - x[i]);
    }
    
    
//     atlag szamolasa (avg=1/N*sum(ln|4-8*x|)--------------------------------
    for(i=0; i<N; i++)
    {
        log_sum += log(abs(4 - 8*x[i]));
    }
    avg = log_sum/N;
    
    cout<<"avarage after "<<N<<" iterations: "<<avg<<endl;
    cout<<"ln(2) = "<<log(2)<<endl;
    
    
//     hisztogram beallitasa--------------------------------------------------
    gsl_histogram * h = gsl_histogram_alloc (BINS);
    gsl_histogram_set_ranges_uniform (h, 0, 1);
    
    
//     hisztogram keszitese x-bol---------------------------------------------
    for(i=0; i<N; i++)
    {
        gsl_histogram_increment (h, x[i]);
    }
    
    
//     kiiras fajlba----------------------------------------------------------
    FILE * hist;
    hist=fopen("hist.dat","w");    
    gsl_histogram_fprintf (hist, h, "%g", "%g");
    fclose(hist);

    /*
    ofstream outfile;
    outfile.open("x.dat");    
    for(i=0; i<N; i++)
    {
        outfile<<x[i]<<endl;
    }
    outfile.close();
    */
    
//     gnuplot parancsok------------------------------------------------------
    ofstream plot;
    plot.open("plot_hist.gp");
    plot << "set xrange [0:1]"<<endl
        <<"set yrange [0:]"<<endl
        <<"set xtics out"<<endl
        <<"set ytics out"<<endl
        <<"set style line 1 lt 4 lw 2 lc rgb \"red\""<<endl
        <<"f(x)=1/(pi*sqrt(x*(1-x)))"<<endl
        <<"p \"hist.dat\" u ($1+($2-$1)/2.0):($3/(($2-$1)*"<<N<<")) w boxes lc rgb \"blue\" notitle, f(x) w lines ls 1 t \"1/(pi*sqrt(x*(1-x)))\" "<<endl;
    plot.close();
    
    return 0; 
}

