#include <iostream>
#include <cmath>
#include <fstream>
using namespace std;
#define N 4
#define dt 0.01
#define tmax 100

//valtozok
int i;
double x[N]={4,0,0,1/sqrt(10)};//kezdeti felt
double t=0,deltaE;


//fuggvenyek
double f(double x[],int j)//derivalt fuggvenyek, a fuggvenyek szama meg kell egyezzen az N dimenzioval!
{
  if (j==0) return x[2];
  else if (j==1) return x[3];
  else if (j==2) return -x[0]*sqrt(pow(pow(x[0],2)+pow(x[1],2),-3));
  else if (j==3) return -x[1]*sqrt(pow(pow(x[0],2)+pow(x[1],2),-3));
}

double E(double x[])//energia szamolo fv
{
  return (pow(x[2],2)+pow(x[3],2))/2-sqrt(pow(pow(x[0],2)+pow(x[1],2),-1));
}

void rk4(double x[], double delta)//runge-kutta lepteto, N dimenzioju bemenetre mukodik, j az egyenleteket indexeli, a bemenet: egyenletrendszer t-ben, kimenet egyenletrendszer t+delta-ban
{
  int j;
  double a[N],b[N],c[N],d[N],tmp1[N],tmp2[N],tmp3[N];
  for(j=0;j<N;j++){tmp1[j]=x[j]+0.5*(a[j]=delta*f(x,j));}
  for(j=0;j<N;j++){tmp2[j]=x[j]+0.5*(b[j]=delta*f(tmp1,j));}
  for(j=0;j<N;j++){tmp3[j]=x[j]+(c[j]=delta*f(tmp2,j));}
  for(j=0;j<N;j++){d[j]=delta*f(tmp3,j);}
  for(j=0;j<N;j++){x[j]+=(a[j]+2*b[j]+2*c[j]+d[j])/6;}
}

//program futas
int main()
{ 
  
  //kimenet beallitas (ha ezeket main-en kivul rakom nem fordul le)
  ofstream outfile;
  outfile.setf(ios::scientific);//kimeneti szamformatum
  outfile.precision(13);//ennyi tizedesig irja ki a szamokat

  outfile.open ("1.dat");
  outfile<<"#ido\tx\ty\tpx\tpy\tE-E0\n";//kimeneti fajl fejlecenek kiirasa
  
  for(i=0;i<100/dt;i++)//diffegyenletrendszer leptetese, lepesenkenti kiiras
  {
    deltaE=E(x)+0.2;//E-E0 
    outfile << t<<"\t"<<x[0]<<"\t"<<x[1]<<"\t"<<x[2]<<"\t"<<x[3]<<"\t"<<deltaE<<"\n";//kiirja az x[] vektort a t időpillanatban    
    rk4(x,dt);//x[](t)--->x[](d+dt)
    t+=dt;//ez a tablazat elso, idot tartalmazo oszlopahoz kell, illetve ha f fugg explicit az idotol (ekkor rk4()-et is modositani kell)
  }
  outfile.close();
  
  return 0;
} 
