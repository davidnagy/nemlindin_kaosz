#include <iostream>
#include <sstream>
#include <cmath>
#include <fstream>
using namespace std;
#define N 3
#define dt 0.01
#define tmax 100


int i;
double t=0;
double x[N];


double f(double x[],int j)	//derivalt fuggvenyek
{
  if (j==0) return x[2]+x[1]+0.2*x[0];//xp
  else if (j==1) return -x[0]+0.2*x[1];//yp
  else if (j==2) return -2*x[2]*(3+x[0])+1;//zp
}

void rk4(double x[], double delta)	//runge-kutta lepteto, N dimenzioju bemenetre mukodik
{
  int j;
  double a[N],b[N],c[N],d[N],tmp1[N],tmp2[N],tmp3[N];
  for(j=0;j<N;j++){tmp1[j]=x[j]+0.5*(a[j]=delta*f(x,j));}
  for(j=0;j<N;j++){tmp2[j]=x[j]+0.5*(b[j]=delta*f(tmp1,j));}
  for(j=0;j<N;j++){tmp3[j]=x[j]+(c[j]=delta*f(tmp2,j));}
  for(j=0;j<N;j++){d[j]=delta*f(tmp3,j);}
  for(j=0;j<N;j++){x[j]+=(a[j]+2*b[j]+2*c[j]+d[j])/6;}
}

int main()
{
  
  cout << "add meg a kezdeti felteteleket: x0 enter y0 enter z0 enter"<<endl;
  for(i=0;i<3;i++)
  {
   cin >> x[i]; 
  }
  
  
  

//filebairas
  ofstream outfile;
  stringstream filename;
  filename << "1a_" << x[0] << x[1]<<x[2]<< ".dat";
  outfile.open( filename.str().c_str());
  
  outfile.setf(ios::scientific);
  outfile.precision(13);
  cout.precision(13);
  
  outfile<<"#ido\tx\ty\tz\n";
  
  for(i=0;i<100/dt;i++)
  {
    outfile << t<<"\t"<<x[0]<<"\t"<<x[1]<<"\t"<<x[2]<<"\n";
    //cout<<scientific<<t<<"\t"<<x[0]<<"\t"<<x[1]<<"\t"<<x[2]<<endl;
    rk4(x,dt);
    t+=dt;
  }
  outfile.close();
  
  return 0;
  
  
  
}