// b feladat
#include <iostream>
#include <sstream>
#include <cmath>
#include <fstream>
using namespace std;
#define N 3
#define dt 0.01
#define tmax 100

double c[5]={2.6,3.5,4,4.18,0};
double f(double x[],int j, double c)	//derivalt fuggvenyek
{
  if (j==0) return -(x[2]+x[1]);//xp
  else if (j==1) return x[0]+0.2*x[1];//yp
  else if (j==2) return 0.2+x[0]*x[2]-c*x[2];//zp
}

void rk4(double x[], double delta, double param)	//runge-kutta lepteto, N dimenzioju bemenetre mukodik
{
  int j;
  double a[N],b[N],c[N],d[N],tmp1[N],tmp2[N],tmp3[N];
  for(j=0;j<N;j++){tmp1[j]=x[j]+0.5*(a[j]=delta*f(x,j,param));}
  for(j=0;j<N;j++){tmp2[j]=x[j]+0.5*(b[j]=delta*f(tmp1,j,param));}
  for(j=0;j<N;j++){tmp3[j]=x[j]+(c[j]=delta*f(tmp2,j,param));}
  for(j=0;j<N;j++){d[j]=delta*f(tmp3,j,param);}
  for(j=0;j<N;j++){x[j]+=(a[j]+2*b[j]+2*c[j]+d[j])/6;}
}

int main()
{
  int i,j;  
  double x[N]={0,0,0};//kezdeti felt
  cout << "add meg a kezdeti felteteleket: x0 enter y0 enter z0 enter"<<endl;
  for(i=0;i<3;i++)
  {
   cin >> x[i]; 
  }
  double t=0;
  
  cout << "mi legyen az utolso, 4.2< parameter?\n";
  cin >> c[4];
  
  

//filebairas
  ofstream outfile;
  
  outfile.setf(ios::scientific);
  outfile.precision(13);
  cout.precision(13);
  
  
  for (j=0;j<5;j++)
  {
    stringstream filename;
    filename << "1b_c" << j << ".dat";
    outfile.open( filename.str().c_str() );
    outfile<<"#ido\tx\ty\tz\tc "<<c[j]<<"\n";
    
  for(i=0;i<100/dt;i++)
  {
    outfile << t<<"\t"<<x[0]<<"\t"<<x[1]<<"\t"<<x[2]<<"\n";
    //cout<<scientific<<t<<"\t"<<x[0]<<"\t"<<x[1]<<"\t"<<x[2]<<endl;
    rk4(x,dt,c[j]);
    t+=dt;
  }
  outfile.close();
  }
  
  return 0;
  
  
  
} 
