gnuplot -persist <<- EOF
set term x11 0
	load "yx.gp"
set term x11 1
	load "pt.gp"
set term x11 2
	load "deltaE.gp"
set term x11 3
	load "xtyt.gp"
set term x11 4
	load "1a.gp"
    pause mouse keypress
EOF
