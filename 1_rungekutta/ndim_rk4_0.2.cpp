#include <iostream>
#include <cmath>
#include <fstream>
using namespace std;
#define N 4
#define dt 0.01
#define tmax 100





double f(double x[],int j)	//derivalt fuggvenyek
{
  if (j==0) return x[2];
  else if (j==1) return x[3];
  else if (j==2) return -x[0]*sqrt(pow(pow(x[0],2)+pow(x[1],2),-3));
  else if (j==3) return -x[1]*sqrt(pow(pow(x[0],2)+pow(x[1],2),-3));
}

double E(double x[])	//energia szamolo fv
{
  return (pow(x[2],2)+pow(x[3],2))/2-sqrt(pow(pow(x[0],2)+pow(x[1],2),-1));
}

void rk4(double x[], double delta)	//runge-kutta lepteto, N dimenzioju bemenetre mukodik
{
  int j;
  double a[N],b[N],c[N],d[N],tmp1[N],tmp2[N],tmp3[N];
  for(j=0;j<N;j++){tmp1[j]=x[j]+0.5*(a[j]=delta*f(x,j));}
  for(j=0;j<N;j++){tmp2[j]=x[j]+0.5*(b[j]=delta*f(tmp1,j));}
  for(j=0;j<N;j++){tmp3[j]=x[j]+(c[j]=delta*f(tmp2,j));}
  for(j=0;j<N;j++){d[j]=delta*f(tmp3,j);}
  for(j=0;j<N;j++){x[j]+=(a[j]+2*b[j]+2*c[j]+d[j])/6;}
}

int main()
{
  int i;
  double x[N];//={4,0,0,1};//kezdeti felt
  double t=0,deltaE;
  x[0]=4;//x
  x[1]=0;//y
  x[2]=0;//px
  x[3]=1/sqrt(10);//py*/

//filebairas
  ofstream outfile;
  outfile.open ("1.dat");
  outfile.setf(ios::scientific);
  outfile.precision(13);
  cout.precision(13);
  
  outfile<<"#ido\tx\ty\tpx\tpy\tE-E0\n";
  
  for(i=0;i<100/dt;i++)
  {
    deltaE=E(x)+0.2;
    outfile << t<<"\t"<<x[0]<<"\t"<<x[1]<<"\t"<<x[2]<<"\t"<<x[3]<<"\t"<<deltaE<<"\n";
    //cout<<scientific<<t<<"\t"<<x[0]<<"\t"<<x[1]<<"\t"<<x[2]<<"\t"<<x[3]<<"\t"<<deltaE<<endl;
    rk4(x,dt);
    t+=dt;
  }
  outfile.close();
  
  return 0;
}