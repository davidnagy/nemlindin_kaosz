//
//  egydim_rk4.cpp
//  RungeKutta4
//
//  Created by David Nagy on 2/14/13.
//  Copyrigt (c) 2013 David Nagy. All rights reserved.
//

#include <iostream>
#include <cmath>
#include <limits>
using namespace std;
#include <fstream>

void write_array_to_file(double* array, int length)
{
    ofstream output("output.txt");
    for(int i=0;i<length;i++)
    {
        output<<array[i]<<endl;
    }
}

double x[100];
//double t[100];
int t0=0;
int x0=1;
double delta_t=0.1;

//x_prime(t)=F(t,x(t))
//dx_per_dt(t)=exp(-2*t)-3*x(t)
double f (double t, double x)
{
    return exp(-2*t)-3*x;
}

int main() 
{
    x[0]=x0;
    double t=t0;
    int i;
    double a,b,c,d;
    cout.precision(15);

    for(i=0;i<=100; i++)
    {     
        a=delta_t*f(t,x[i]);
        b=delta_t*f(t+delta_t/2,x[i]+a/2);
        c=delta_t*f(t+delta_t/2,x[i]+b/2);
        d=delta_t*f(t+delta_t,x[i]+c);
        x[i+1]=x[i]+(a+2*b+2*c+d)/6;
        //cout<<fixed<<t<<endl;
        cout<<fixed<<x[i]<<endl;
        t=t+delta_t;
    }
write_array_to_file(x, 100);
    

}