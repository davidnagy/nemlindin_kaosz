for i in $(ls standardmap_*.dat)
do
j=${i%.*}
gnuplot <<- EOF
set t png size 1280,1280
set size square
set key off
set o "$j.png"
set xla "x"
set yla "p"
p [0:2*pi][0:2*pi]"$i" u 1:2:3  w dots palette
set o
EOF
done
