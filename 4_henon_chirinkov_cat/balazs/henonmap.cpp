// kaosz es nem lin.din. 4. feladat - Rudd Balazs
// Henon lekepezes - henonmap.cpp
/*
 * forditas: g++ -o fajlnev henonmap.cpp
 * abrazolas: gnuplot p "./henonmap_j.dat" u 1:2:3  w dots palette, ahol j az iteraciok szama
 * N az iteraciok szama
 */ 
#include <iostream>
#include <cmath>
#include <fstream>
#include <sstream>
#include <ctime>
#include <cstdlib>

#define N 1000
#define a 1.4
#define b 0.3

using namespace std;

int i,j;
double state[2]; 
int color=0;

void henon_stepper(double state[2]);

int main()
{
    j=1;

    while (j<1001)
    {
        ofstream outfile;
        stringstream filename;
        filename << "henonmap_" <<j<< ".dat";
        
        outfile.open( filename.str().c_str() );
        
        state[0]=0.1;
        state[1]=-0.2;
        outfile<<state[0]<<" "<<state[1]<<" "<<color<<endl;

        for (i=0; i<j; i++)
            {
                henon_stepper(state);
                outfile<<state[0]<<" "<<state[1]<<" "<<color<<endl;
                color++;
            }
            outfile.close();
    // cout<<j<<endl<<flush;
        j*=10;
    }
    
    return 0;   
}

void henon_stepper(double state[2])
{
    double tmp=state[0];
    state[0]=state[1]+1-a*pow(state[0],2);
    state[1]=b*tmp;
}