// kaosz es nem lin.din. 4. feladat - Rudd Balazs
// chirinkov lekepezes - standardmap.cpp
/*
 * forditas: g++ -o fajlnev standardmap.cpp
 * abrazolas: gnuplot p "./standardmap_K.dat" u 1:2:3  w dots palette, ahol K a futas kozben bekert parameter
 * N az iteraciok szama, n*n pedig a 2pi*2pi fazisterbeli tartomanyban a kezdeti ertekek szamat adja meg.
 */
#include <iostream>
#include <cmath>
#include <fstream>
#include <sstream>
#include <ctime>
#include <cstdlib>

#define N 350
#define n 35

using namespace std;

int i,j,k;
int color=0;
double K;
double state[2];    
double delta=2*M_PI/n;

void chirinkov_stepper(double state[2]);


int main()
{
    
    cout<<"add meg a K kontrol parametert:"<<endl;
    cin>>K;
    
    
    stringstream filename;
    filename << "standardmap_" << K << ".dat";
    ofstream outfile;
    outfile.open( filename.str().c_str() );
    
    for (k=0; k<n; k++)
    {
        cout<<"."<< flush;
        for (j=0; j<n; j++)
        {
            state[0]=j*delta;
            state[1]=k*delta;
            color+=1;
            outfile<<state[1]<<" "<<state[0]<<" "<<color<<endl;
            for (i=0; i<N; i++)
            {
                chirinkov_stepper(state);
                outfile<<state[1]<<" "<<state[0]<<" "<<color<<endl;
            }
        }
    }
    outfile.close();
    cout<<endl;
    return 0;
}

void chirinkov_stepper(double state[2])
{
    state[0]=fmod(state[0],2*M_PI)+K*sin(fmod(state[1],2*M_PI));
    state[0]=fmod(state[0],2*M_PI);
    state[1]=fmod(state[1],2*M_PI)+state[0];
    state[1]=fmod(state[1],2*M_PI);
    if (state[0]<0)
    {
        state[0]+=2*M_PI;
    }
    if (state[1]<0)
    {
        state[1]+=2*M_PI;
    }
}
