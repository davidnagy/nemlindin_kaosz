for i in $(ls henonmap_*.dat)
do
j=${i%.*}
gnuplot <<- EOF
set t png size 1280,1280
set key off
set o "$j.png"
p "$i" u 1:2 w point pt 7 ps 0.5  
set o
EOF
done 
