//
//  main.cpp
//  Chirinkov map
//
//  compile with
//  g++ -larmadillo -o run -O1 chirinkov_map.cpp
//  ./run
//
//  Created by David Nagy on 3/16/13.
//  Copyright (c) 2013 David Nagy. All rights reserved.
//

#include <iostream>
#include <armadillo>
#include <math.h>

using namespace std;
using namespace arma;

//    double K=5;
//    double K=0.5;
    double K=0.971635;
//    double K=0.6;
    int steps=10000;
    int num_of_initvs=100;

mat develop_state(mat state)
{
    mat newstate=zeros<mat>(1,2);
    newstate(0)=state(0)+K*sin(state(1)); //update p
    newstate(1)=newstate(0)+state(1); //update x
    while(newstate(0) >= 2*M_PI) newstate(0) -= 2*M_PI;
    while(newstate(0) < 0) newstate(0) += 2*M_PI;
    while(newstate(1) >= 2*M_PI) newstate(1) -= 2*M_PI;
    while(newstate(1) < 0) newstate(1) += 2*M_PI;
 //   cout<<state;
    return newstate;
}

int main()
{
    mat state=zeros<mat>(steps*num_of_initvs,2);
    mat init_value_matrix(num_of_initvs,2);

    init_value_matrix.randu();
    init_value_matrix*=2*M_PI;

    for (int j=0; j<num_of_initvs-1; j++)
    {
        state(j*steps,0)=init_value_matrix(j,0);
        state(j*steps,1)=init_value_matrix(j,1);

        for (int i=0; i<steps-1; i++)
        {
            state.row(j*steps+i+1)=develop_state(state.row(j*steps+i));
        }
    }
    
    //cout << state;
    state.save("henon.dat", raw_ascii);

    //gnuplot plot
    FILE* gnuplotPipe;
    gnuplotPipe = popen("gnuplot","w");
    fprintf(gnuplotPipe,"plot \"henon.dat\" u 2:1 with dots\n");
    fflush(gnuplotPipe);
    printf("press enter to close...");        
    getchar();        
    fprintf(gnuplotPipe,"exit \n");
    return 0;
}