//
//  main.cpp
//  Henon map
//
//  compile with
//  g++ -larmadillo -o run -O1 henon_map.cpp
//  ./run
//
//  Created by David Nagy on 3/16/13.
//  Copyright (c) 2013 David Nagy. All rights reserved.
//

#include <iostream>
#include <armadillo>

using namespace std;
using namespace arma;

    double a=1.4;
    double b=0.3;
    int steps=100000;

mat develop_state(mat state)
{
    mat newstate=zeros<mat>(1,2);
    newstate(0)=state(1)+1-a*state(0)*state(0); //update x
    newstate(1)=b*state(0); //update y
    return newstate;
}

int main()
{
    mat state=zeros<mat>(steps,2);

    state(0,0)=1;
    state(0,1)=1;

    for (int i=0; i<steps-1; i++)
    {
        state.row(i+1)=develop_state(state.row(i));
    }
    
    //cout << state;
    state.save("henon.dat", raw_ascii);

    //gnuplot plot
    FILE* gnuplotPipe;
    gnuplotPipe = popen("gnuplot","w");
    fprintf(gnuplotPipe,"plot \"henon.dat\" with dots\n");
    fflush(gnuplotPipe);
    printf("press enter to close...");        
    getchar();        
    fprintf(gnuplotPipe,"exit \n");
    return 0;
}