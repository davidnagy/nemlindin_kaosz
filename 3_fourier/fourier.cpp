//
//  main.cpp
//  FFT
//
//  Created by David Nagy on 3/6/13.
//  Copyright (c) 2013 David Nagy. All rights reserved.
//

#include <iostream>
#include <stdio.h>
#include <fstream>
#include <gsl/gsl_fft_real.h>
#include <gsl/gsl_fft_complex.h>

using namespace std;

int main ()
{
    int N=262144;
    
    //read data
   ifstream infile;
    double data[N];
    infile.open("1x.dat");
    for (int i=0;i<N;i++){
        infile>>data[i];
        }
    infile.close(); 
    
    //transform
    gsl_fft_real_radix2_transform(data,1,N);
    
     //sort
     double * real_part = NULL;
     real_part = new (nothrow) double [N];
     double * imaginary_part = NULL;
     imaginary_part = new (nothrow) double [N];
     
     real_part[0]=data[0];
     imaginary_part[0]=0;
     double re=0, im=0;
     int k;
     
     for(k=1;k<N-k;k++)
     {
     re=data[k];
     im=data[N-k];
     real_part[k]=re;
     real_part[N-k]=re;
     imaginary_part[k]=im;
     imaginary_part[N-k]=-im;
     
     }
     if(k==N-k)
     {
     real_part[k]=data[N-1];
     imaginary_part[k]=0;
     }
     
     double * abs_squared = NULL;
     abs_squared = new (nothrow) double [N];
     for(k=0;k<N;k++)
     {
     abs_squared[k]=real_part[k]*real_part[k]+imaginary_part[k]*imaginary_part[k];
     }
    
    /*for(int i=0; i<=N; i++) {
        cout<<abs_squared[i]<<endl;
    }*/

    //filebairas
	  ofstream outfile;
	  outfile.open ("fftbolygo.dat");
	  outfile.setf(ios::scientific);
	  outfile.precision(13);
	  cout.precision(13);
	  for(int j=0;j<3000;j++) {
	    outfile<<abs_squared[j]<<endl;
	  }
	  outfile.close();
	
    
    //gnuplot plot
	FILE* gnuplotPipe;
	gnuplotPipe = popen("gnuplot","w");
	fprintf(gnuplotPipe,"set logscale y\n");
	fprintf(gnuplotPipe,"plot \"fftbolygo.dat\" w lines \n");
    fflush(gnuplotPipe);
    printf("press enter to continue...");
    getchar();
    fprintf(gnuplotPipe,"exit \n");
    
    
    return(0);
}
